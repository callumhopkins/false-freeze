use std::time::{SystemTime, UNIX_EPOCH};

use rand::{RngCore, SeedableRng};
use rand_pcg::Pcg64;

use crate::Rand;

pub struct Seed(Pcg64);

impl Seed {
    pub fn new(seed: usize) -> Self {
        Self(Pcg64::seed_from_u64(seed as u64))
    }
}

impl Rand for Seed {
    fn next(&mut self) -> usize {
        self.0.next_u64() as usize
    }
}

impl Default for Seed {
    fn default() -> Self {
        let duration_since_epoch = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("system time set before UNIX_EPOCH");

        let timestamp = duration_since_epoch.as_secs();

        Self::new(timestamp as usize)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn next_should_ret_with_same_seed() {
        let mut seed_one = Seed::new(0);
        let mut seed_two = Seed::new(0);

        assert_eq!(seed_one.next(), seed_two.next());
    }

    #[test]
    fn next_should_ret_with_diff_seed() {
        let mut seed_one = Seed::new(0);
        let mut seed_two = Seed::new(1);

        assert_ne!(seed_one.next(), seed_two.next());
    }

    #[test]
    fn def_ctor_should_use_unix() {
        let duration_since_epoch = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("system time set before UNIX_EPOCH");

        let timestamp = duration_since_epoch.as_secs();

        let mut seed_one = Seed::default();
        let mut seed_two = Seed::new(timestamp as usize);

        assert_eq!(seed_one.next(), seed_two.next())
    }
}
