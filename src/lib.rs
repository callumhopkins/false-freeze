pub mod mock;
pub mod seed;

pub type Mock = self::mock::Mock;
pub type Seed = self::seed::Seed;

pub trait Rand {
    fn next(&mut self) -> usize;
}
