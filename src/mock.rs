use crate::Rand;

pub struct Mock {
    curr: usize,
    step: usize,
}

impl Mock {
    pub fn new(init: usize, step: usize) -> Self {
        Self { curr: init, step }
    }
}

impl Rand for Mock {
    fn next(&mut self) -> usize {
        self.curr += self.step;
        self.curr - self.step
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn next_should_ret_init_val() {
        {
            let mut mock = Mock::new(0, 0);

            assert_eq!(mock.next(), 0);
        }

        {
            let mut mock = Mock::new(1, 0);

            assert_eq!(mock.next(), 1);
        }
    }

    #[test]
    fn next_should_inc_step_val() {
        {
            let mut mock = Mock::new(0, 1);

            assert_eq!(mock.next(), 0);
            assert_eq!(mock.next(), 1);
        }

        {
            let mut mock = Mock::new(0, 2);

            assert_eq!(mock.next(), 0);
            assert_eq!(mock.next(), 2);
        }
    }
}
